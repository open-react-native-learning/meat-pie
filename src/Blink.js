import React from 'react';
import { Text } from 'react-native';
import { STYLES } from 'Styles';
import PropTypes from 'prop-types';

export default class Blink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isShowingText: true};

    setInterval(() => {
      this.setState(previousState => {
        return { isShowingText: !previousState.isShowingText };
      });
    }, 1000);
  }

  render() {
    let display = this.state.isShowingText ? this.props.text : 'WANT MEAT PIE';
    return (
      <Text style={STYLES.BLINKTEXT}>{display}</Text>
    );
  }
}

Blink.propTypes = {
  text: PropTypes.string.isRequired,
};
