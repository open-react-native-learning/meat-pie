import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

export default class Greeting extends React.Component {
  render() {
    return (
      <Text>Hello {this.props.name}!</Text>
    );
  }
}

Greeting.propTypes = {
  name: PropTypes.string.isRequired,
};
