import { StyleSheet } from 'react-native';
import { COLORS } from 'Colors';

export const STYLES = StyleSheet.create({
  CONTAINER: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: COLORS.BACKGROUND,
  },
  BLINKTEXT: {
    fontSize: 24,
  },
});
