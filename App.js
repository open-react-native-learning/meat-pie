import React from 'react';
import { View } from 'react-native';
import { STYLES } from 'Styles';
import Blink from 'Blink';

export default class LotsOfGreetings extends React.Component {
  render() {
    return (
      <View style={STYLES.CONTAINER}>
        <Blink text='Blinking this and that'/>
        <Blink text='Looking here and there'/>
        <Blink text='Going up and down'/>
        <Blink text='Moving from side to side'/>
      </View>
    );
  }
}
